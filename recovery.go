package recovery

import (
	"bytes"
	"fmt"
	"gitlab.com/horo-go/errorr"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime"
)

type Recovery struct {
	ErrorLog *log.Logger
}

func NewRecovery() *Recovery {
	return &Recovery{
		ErrorLog: log.New(os.Stderr, "HORO-RECOVERY  ", log.LstdFlags),
	}
}

func (rc *Recovery) logf(format string, args ...interface{}) {
	if rc.ErrorLog != nil {
		rc.ErrorLog.Printf(format, args...)
	} else {
		log.Printf(format, args...)
	}
}

var (
	dunno     = []byte("???")
	centerDot = []byte("·")
	dot       = []byte(".")
	slash     = []byte("/")
)

func (rc *Recovery) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				if br, ok := err.(*errorr.BadRequestError); ok {
					http.Error(w, br.Error(), http.StatusBadRequest)
					return
				}

				httprequest, _ := httputil.DumpRequest(r, false)

				if nr, ok := err.(*errorr.NonRecoveryError); ok {
					stack := stack(0)
					rc.logf("panic cannot be recovered:\n%s\n%s\n%s", err, string(httprequest), stack)
					panic(nr)
				}

				stack := stack(4)
				rc.logf("panic recovered:\n%s\n%s\n%s", err, string(httprequest), stack)

				http.Error(w, "internal server error", http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

// stack returns a nicely formatted stack frame, skipping skip frames.
func stack(skip int) []byte {
	buf := new(bytes.Buffer) // the returned data
	// As we loop, we open files and read them. These variables record the currently
	// loaded file.
	var lines [][]byte
	var lastFile string
	for i := skip; ; i++ { // Skip the expected number of frames
		pc, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		// Print this much at least.  If we can't find the source, it won't show.
		fmt.Fprintf(buf, "%s:%d (0x%x)\n", file, line, pc)
		if file != lastFile {
			data, err := ioutil.ReadFile(file)
			if err != nil {
				continue
			}
			lines = bytes.Split(data, []byte{'\n'})
			lastFile = file
		}
		fmt.Fprintf(buf, "\t%s: %s\n", function(pc), source(lines, line))
	}
	return buf.Bytes()
}

// source returns a space-trimmed slice of the n'th line.
func source(lines [][]byte, n int) []byte {
	n-- // in stack trace, lines are 1-indexed but our array is 0-indexed
	if n < 0 || n >= len(lines) {
		return dunno
	}
	return bytes.TrimSpace(lines[n])
}

// function returns, if possible, the name of the function containing the PC.
func function(pc uintptr) []byte {
	fn := runtime.FuncForPC(pc)
	if fn == nil {
		return dunno
	}
	name := []byte(fn.Name())
	// The name includes the path name to the package, which is unnecessary
	// since the file name is already included.  Plus, it has center dots.
	// That is, we see
	// 	runtime/debug.*T·ptrmethod
	// and want
	// 	*T.ptrmethod
	// Also the package path might contains dot (e.g. code.google.com/...),
	// so first eliminate the path prefix
	if lastslash := bytes.LastIndex(name, slash); lastslash >= 0 {
		name = name[lastslash+1:]
	}
	if period := bytes.Index(name, dot); period >= 0 {
		name = name[period+1:]
	}
	name = bytes.Replace(name, centerDot, dot, -1)
	return name
}
